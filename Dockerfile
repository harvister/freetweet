FROM python:3

# twint
RUN pip install --trusted-host pypi.python.org twint

# node
ENV NODE_VERSION=10.15.2
RUN wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
ENV NVM_DIR=/root/.nvm
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION}
ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"

# sqlite
RUN apt-get update
RUN apt-get install -y sqlite3

# # npm install
WORKDIR /app
COPY package.json package-lock.json /app/
RUN npm i

# run
COPY . /app
CMD [ "bash", "run.sh"]
