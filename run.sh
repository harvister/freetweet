# load .env file if exists
if [ -f .env ]; then
    set -a; . .env; set +a
fi

if [ -s $dry_run ]; then
   dry_run=true
fi

if [ -s $retweets ]; then
   retweets=false
fi

if [ -s $twitter_account ]; then
    echo must provide twitter_account
    exit 1
fi

twitter_account=${twitter_account,,}

if [ -s $limit ]; then
   limit=1
fi

echo twitter_account: $twitter_account
echo limit: $limit
if $dry_run; then
    echo "dry run"
else
    echo "live run"
fi

mkdir -p databases
if $retweets; then
    echo "scraping tweets and retweets"
    res=$(twint -u $twitter_account --retweets --database ./databases/$twitter_account.db --limit $limit --count)
else
    echo "scraping tweets"
    res=$(twint -u $twitter_account --database ./databases/$twitter_account.db --limit $limit --count)
fi

sqlite3 ./databases/$twitter_account.db <<END_SQL
update tweets
set id_str = 0
where id not in (
select id
from tweets
order by id desc
limit $limit
);
END_SQL
echo limiting tweets

count=$(sqlite3 ./databases/$twitter_account.db <<END_SQL
select count(*)
from tweets
where id_str != 0;
END_SQL
)

echo "$count tweet(s) to be pushed"

if [ $count -lt 1 ]; then
    exit
fi

./node_modules/ts-node/dist/bin.js freetweet.ts -u $twitter_account --verbose=$verbose --dry-run=$dry_run \
    --tweeted-by=$tweeted_by
