require('dotenv').config();

const argv = require('argv');
import path from 'path';
import sqlite3 from 'sqlite3';
import Masto, { CreateStatusParams } from 'masto';

class Logger {
  constructor(private isVerbose: boolean) { }

  log(message?: any, ...optionalParams: any) {
    console.log(message, ...optionalParams);
  }

  verbose(message?: any, ...optionalParams: any) {
    if (!this.isVerbose) return;
    this.log(message, ...optionalParams);
  }
}

const config = getConfig();
const logger = new Logger(config.verbose);

/*
 * processTweet (err, row)
 * Middleware to process one tweet in a stream of tweets arriving from sqlite3.
 */
function processTweet(tweet: TweetRow, mastoInstance: Masto): Promise<void> {
  if (tweet.id_str === '0') {
    return Promise.resolve();
  }

  let post: CreateStatusParams = {
    visibility: 'public',
    status: tweet.tweet
  };
  if (config.showTimestamp) {
    post.status = post.status + `\n\n${tweet.date}T${tweet.time}`;
  }
  if (config.showTwitterLink) {
    post.status = post.status + `\n${tweet.link}`;
  }
  if (tweet.screen_name.toLowerCase() === config.username) {
    if (config.showTweetedBy) {
      post.status = `${tweet.name} tweeted:\n\n${post.status}`;
    }
  } else {
    post.status = `${config.username} retweeted ${tweet.name}:\n\n${post.status}`;
  }

  logger.verbose('processing tweet', { id_str: tweet.id_str, status: post.status.slice(0, 75) });

  if (config.dryRun) {
    return Promise.resolve();
  }

  return mastoInstance
    .createStatus(post)
    .then((data) => {
      logger.log('mirrored tweet', tweet.id_str);
      logger.verbose({ id: tweet.id_str, status: data });
    });
};

/*
 * updateTweets ( )
 * Process all pending updated tweet records by setting their id_str value to 0
 * in the sqlite3 db.
 */
function updateTweets(database: sqlite3.Database, processedTweetIds: string[], updateQuery: string): Promise<void> {
  console.log('updating processed tweets');
  const tweetId = processedTweetIds.shift();
  if (!tweetId) {
    return Promise.resolve();
  }

  logger.verbose('clearing tweet', { id: tweetId });
  return new Promise((resolve, reject) => {
    database.run(updateQuery, [tweetId], function (err) {
      if (err) {
        console.error(err.message);
        return reject(err);
      }
      return resolve();
    });
  })
    .then(() => {
      return updateTweets(database, processedTweetIds, updateQuery);
    });
};

async function runProcess(tweets: TweetRow[], mastoInstance: Masto): Promise<string[]> {
  const processedTweetIds: string[] = [];
  for (const tweet of tweets) {
    try {
      await processTweet(tweet, mastoInstance);
      processedTweetIds.push(tweet.id_str);
    } catch (err) {
      console.error(err);
    }
  }
  return processedTweetIds;
}

function getTweets(database: sqlite3.Database, getTweetsQuery: string): Promise<TweetRow[]> {
  return new Promise((resolve, reject) => {
    database.all(getTweetsQuery, (err, rows: TweetRow[]) => {
      if (err) {
        return reject(err);
      }
      resolve(rows);
    });
  });
}

/*
 * Main Entry Point
 */
(async function main() {
  console.log('connecting to Gab Social');
  const mastoInstance = await Masto.login({
    uri: config.instanceUrl,
    accessToken: process.env.GAB_SOCIAL_ACCESS_TOKEN,
  });
  logger.verbose('connected to Gab Social');

  const dbFilename = path.join(config.dbPath, `${config.username}.db`);
  console.log('opening sqlite3 db', { dbFilename: dbFilename });
  const database = await openDatabase(dbFilename);

  const tweets = await getTweets(database, config.queries.getTweets);
  const processedTweetIds = await runProcess(tweets, mastoInstance);
  console.log(`${processedTweetIds.length} tweets processed`);

  await updateTweets(database, processedTweetIds, config.queries.updateTweet);
  logger.verbose('processed tweets updated');

  console.log('disconnecting from sqlite3 db', dbFilename);
  await closeDatabase(database);
  logger.verbose('disconnected from sqlite3 db', { dbFilename: dbFilename });

})();

function openDatabase(dbFilename: string): Promise<sqlite3.Database> {
  return new Promise((resolve, reject) => {
    const database: sqlite3.Database = new sqlite3.Database(dbFilename, sqlite3.OPEN_READWRITE, (err) => {
      if (err) {
        return reject(err);
      }
      return resolve(database);
    });
  });
}

function closeDatabase(database: sqlite3.Database): Promise<void> {
  return new Promise((resolve, reject) => {
    database.close((err) => {
      if (err) {
        return reject(err);
      }
      resolve();
    });
  });
}

function getConfig() {
  argv.option({
    name: 'username',
    short: 'u',
    description: 'The username of the Twitter user to be scraped.',
    type: 'string'
  });

  argv.option({
    name: 'dry-run',
    short: 'd',
    description: 'Process tweets but don\'t create statuses or update tweet databases.',
    type: 'boolean'
  });

  argv.option({
    name: 'verbose',
    short: 'v',
    description: 'Turn on additional program output useful for debugging.',
    type: 'boolean'
  });

  argv.option({
    name: 'tweeted-by',
    description: "Add 'tweeted by' information for every tweet.  Ex: getongab tweeted: ...",
    type: 'boolean'
  });

  const internalOptions = argv.run().options;

  if (!internalOptions.username) {
    process.stderr.write('must specify Twitter username to track\n');
    process.exit(1);
  }

  return {
    username: internalOptions.username.toLowerCase() as string,
    verbose: internalOptions.verbose as boolean,
    showTweetedBy: internalOptions['show_tweeted_by'] as string,
    dryRun: internalOptions['dry-run'] as boolean,
    dbPath: process.env.TWINT_DB_PATH || './',
    instanceUrl: process.env.GAB_SOCIAL_INSTANCE_URL || 'https://gab.com',
    showTimestamp: process.env.show_timestamp === "true",
    showTwitterLink: process.env.show_twitter_link === "true",
    queries: {
      getTweets: `
            SELECT
                id_str,
                name,
                screen_name,
                tweet,
                date,
                time,
                link
            FROM
                tweets
            WHERE
                id_str != 0
        `,
      updateTweet: `UPDATE tweets SET id_str = 0 where id_str = ?`
    }
  };
}

type TweetRow = {
  id_str: string;
  tweet: string;
  date: string;
  time: string;
  link: string;
  screen_name: string;
  name: string;
};
